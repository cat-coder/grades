import pandas

df = pandas.read_csv('../GEDI_TEE_2024_LAB_2_Respostes_al_formulari_1.csv', delimiter = ',')

print(33*"-")
for i in range(0,len(df.index)):
    print(33*"-")
    row = df.iloc[i, :]
    students = [ row[2], row[3] ]
    print("Grading : ", students)
    
    nota = 0
    resistencias = [750,33,560,47]
    resistencias_medidas = [row[4],row[5],row[6],row[7]]
    porcentajes = [ row[8],row[9],row[10],row[11]] 
    baterias = [ row[12], row[13] ]

    print("resistencias : ", resistencias)
    print("resistencias medidas : ", resistencias_medidas)
    print("porcentajes : ", porcentajes)
    print("baterias : ", baterias)
    
    def inRange(resistencia, resistencia_medida):
        if resistencia - (resistencia * 0.05) <= resistencia_medida <= resistencia + (resistencia *0.05):
            return True

    def percentage(percentage):
        if percentage <= 5:
            return True
    
    def bat(baterias):
        if ( abs(baterias[0] - 9) < 1) and ( abs(baterias[1] - 6) < 1):
            return True

    # resistencias medidas
    for j in range(4):
         if inRange(resistencias[j],resistencias_medidas[j]):
             print(1)
    # percentages
    for k in range(4):
        if percentage(porcentajes[k]):
            print(1)
    # baterias
    for l in range(2):
        if bat(baterias):
            print("b")

